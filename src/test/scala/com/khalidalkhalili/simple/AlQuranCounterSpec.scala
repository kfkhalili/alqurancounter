package com.khalidalkhalili.simple

import com.khalidalkhalili.simple.AlQuranCounter._
import org.scalatest.FlatSpec

object AlQuranCounterSpec extends FlatSpec {

  val filePath = "data/quran-simple.txt"
  final val quranTxt = parseQuran(filePath)
  val qMap: Map[String, Int] = extractQuranicWords(quranTxt)

  "qCounter" should "count the words in the Quran correctly" in {
    assert(qMap.size == 17575)
  }

  "qCounter" should "return correct counts for example words" in {
    assert(qMap("الْهُدَى") == 22)
    assert(qMap("هُدًى") == 27)
    assert(qMap("هُدَايَ") == 2)
    assert(qMap("لِلْمَلَائِكَةِ") == 9)
  }
}
