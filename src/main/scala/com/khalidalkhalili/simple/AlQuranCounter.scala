package com.khalidalkhalili.simple


import scala.collection.immutable.ListMap
import scala.io.Source

object AlQuranCounter extends App {
  val filePath = "data/quran-simple.txt"
  def quranText: Iterator[String] = parseQuran(filePath)


  def parseQuran(filePath: String): Iterator[String] = {
    val src = Source.fromFile(filePath)
    src.getLines
  }

  def extractQuranicWords(text: Iterator[String]): Map[String, Int] = {
    val pattern = "[^\\u0621-\\u063A\\u0641-\\u0655]"
    quranText.flatMap(_.split(pattern))
      .foldLeft(Map.empty[String, Int]) {
        (counts, word) => counts + (word -> (counts.getOrElse(word, 0) + 1))
      }
  }
  val countMap = ListMap(extractQuranicWords(quranText).toSeq.sortWith(_._2 > _._2):_*)
  println(countMap.take(50).mkString("\n").toSeq) //prints top 50

  def suraFinder(quranText: Seq[String]): Unit = {
    val textEnd = if (quranText.indexOf("") > 0) quranText.indexOf("") - 1 else quranText.length

    val indices = quranText.zipWithIndex.filter(_._1.startsWith("بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم")).map(_._2).toList
    val slices = indices zip (indices.tail ++ Seq(textEnd))
    val suraRegex = "بِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيم (.*)".r
    val surasWithOpening = slices.map(pair => quranText.slice(pair._1, pair._2).mkString("\n"))
    }

}